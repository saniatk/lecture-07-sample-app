// utils.js

var Utils = {
    creteElement: function (tagName, parentElement, className) {
        var element = parentElement.ownerDocument.createElement(tagName);
        parentElement.appendChild(element);
        if (typeof(className) == 'string') {
            element.className = className;
        }
        return element;
    },
    getParams: function () {
        var params = {};
        window.location.search.substr(1).split('&').forEach(function (param) {
            var split = param.split('=');
            params[split[0]] = split[1];
        });
        return params;
    }
};