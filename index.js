// index.js

window.addEventListener('load', function () {
    var ul = Utils.creteElement('ul', document.body);

    Object.keys(bikes).forEach(function (bikeId) {
        var li = Utils.creteElement('li', ul), a = Utils.creteElement('a', li), bike = bikes[bikeId];
        a.textContent = bike.vendor + ' ' + bike.name;
        a.href = './bike.html?bikeId=' + bikeId;
    });
});