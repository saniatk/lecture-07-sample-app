// bikes.js

var bikes = {
    1: {
        vendor: 'Yamaha',
        name: 'XT1200ZE Super Tenere ES',
        image: '3507.jpg'
    },
    2: {
        vendor: 'Honda',
        name: 'XL700V Transalp',
        image: '1701.jpg'
    },
    3: {
        vendor: 'Suzuki',
        name: 'V-Strom 1000',
        image: '3387.jpg'
    }
};